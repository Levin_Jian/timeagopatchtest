<?php

/**
 * @file
 * A flexible, configurable date filter.
 * This filter combines multiple date filters into a single filter
 * where all fields are controlled by the same date and can be combined with either AND or OR.
 */
class date_views_time_ago_filter_handler extends views_handler_filter_numeric {

    function init(&$view, &$options) {
        parent::init($view, $options);
        module_load_include('inc', 'date_api', 'date_api_sql');
        if (empty($this->view->date_info)) {
            $this->view->date_info = new stdClass();
        }
        if (empty($this->view->date_info->date_fields)) {
            $this->view->date_info->date_fields = array();
        }
        $this->view->date_info->date_fields = array_merge($this->view->date_info->date_fields, $this->options['date_fields']);

        $this->date_handler = new date_sql_handler(DATE_UNIX);
        $this->date_handler->granularity = isset($options['granularity']) ? $options['granularity'] : 'year';
        $this->format = $this->date_handler->views_formats($this->options['granularity'], 'sql');
        // Identify the base table for this field.
        // It will be used to call for the right query field options.
        $this->base_table = $this->table;
    }

    // Set default values for the date filter.
    function option_definition() {
        $options = parent::option_definition();
        $options['date_fields'] = array('default' => array());
        $options['date_method'] = array('default' => 'OR');
        $options['date_group'] = array('default' => 'date');
        $options['granularity'] = array('default' => 'year');
        return $options;
    }

    function negateOperator($operator) {
        $search = array(">", "<");
        $replace = array("<", ">");
        return str_replace($search, $replace, $operator);
    }

    function addFilterintoDateGroup_opsimplehandling(&$group, &$field, &$negOperator, &$placeholder) {
        $value = $this->calculteTimeAgo($this->value['value']);
        $comp_date = new DateObject($value, date_default_timezone(), $this->format);
        $field = $this->date_handler->sql_field($field, NULL, $comp_date);
        $field = $this->date_handler->sql_format($this->format, $field);
        $this->query->add_where_expression($group, "$field $negOperator  $placeholder", array($placeholder => $value));
    }

    function addFilterintoDateGroup_opbetweenhandling(&$group, &$field, &$placeholder_min, &$placeholder_max) {
        $min_value = $this->calculteTimeAgo($this->value['min']);
        $min_comp_date = new DateObject($min_value, date_default_timezone(), $this->format);
        $max_value = $this->calculteTimeAgo($this->value['max']);
        $max_comp_date = new DateObject($max_value, date_default_timezone(), $this->format);
        $field_min = $this->date_handler->sql_field($field, NULL, $min_comp_date);
        $field_min = $this->date_handler->sql_format($this->format, $field_min);
        $field_max = $this->date_handler->sql_field($field, NULL, $max_comp_date);
        $field_max = $this->date_handler->sql_format($this->format, $field_max);
        if ($this->operator == 'between') {
            $this->query->add_where_expression($group, "$field_min <= $placeholder_min AND $field_max >= $placeholder_max", array($placeholder_min => $min_value,
                $placeholder_max => $max_value));
            return;
        }

        $this->query->add_where_expression($group, "$field_min > $placeholder_min OR $field_max < $placeholder_max", array($placeholder_min => $min_value,
            $placeholder_max => $max_value));
    }

    function addFilterintoDateGroup($function, $field) {
        $group = $this->options['date_group'];
        $negOperator = $this->negateOperator($this->operator);
        $placeholder = $this->placeholder();
        if ($function == 'op_simple') {
            $this->addFilterintoDateGroup_opsimplehandling($group, $field, $negOperator, $placeholder);
            return;
        }
        $placeholder_min = $this->placeholder();
        $placeholder_max = $this->placeholder();
        $this->addFilterintoDateGroup_opbetweenhandling($group, $field, $placeholder_min, $placeholder_max);
    }

    function op_between($field) {
        $this->date_combine_conditions('op_between');
    }

    function op_simple($field) {
        $this->date_combine_conditions('op_simple');
    }

    function calculteTimeAgo($value) {
        $date = new DateTime();
        $date->modify('-' . $value . $this->options['granularity']);
        $value = $date->format($this->format);
//        $value = $date->format('Y-m-d H:i:s');
        return $value;
    }

    /**
     * Combines multiple date WHERE expressions into a single WHERE expression.
     *
     * @param string $function
     *   The function name to use to add individual conditions. Either 'op_simple'
     *   or 'op_between'.
     */
    protected function date_combine_conditions($function) {
        $this->get_query_fields();
        if (empty($this->query_fields)) {
            return;
        }

        // Create a custom filter group for the conditions.
        $this->query->set_where_group($this->options['date_method'], $this->options['date_group']);
        // Add each condition to the custom filter group.
        foreach ((array) $this->query_fields as $query_field) {
            $field = $query_field['field'];
            $this->date_handler = $query_field['date_handler'];

            // Respect relationships when determining the table alias.
            if ($field['table_name'] != $this->table || !empty($this->relationship)) {
                $this->related_table_alias = $this->query->ensure_table($field['table_name'], $this->relationship);
            }
            $table_alias = !empty($this->related_table_alias) ? $this->related_table_alias : $field['table_name'];
            $field_name = $table_alias . '.' . $field['field_name'];

            // Call the appropriate function, either 'op_between' or 'op_simple'.
            //override the parent, as we need to include the filter in a different group
            $this->addFilterintoDateGroup($function, $field_name);
        }

        // Gather all of the condition strings and their placeholders.
        $conditions = array();
        $placeholders = array();
        foreach ($this->query->where[$this->options['date_group']]['conditions'] as $condition) {
            $conditions[] = $condition['field'];
            $placeholders += $condition['value'];
        }

        // Remove the conditions from the custom filter group.
        unset($this->query->where[$this->options['date_group']]);

        // Combine all of the conditions into one string.
        $conditions = implode(' ' . $this->options['date_method'] . ' ', $conditions);

        // Add it to the filter group chosen in the Views UI.
        $this->query->add_where_expression($this->options['group'], $conditions, $placeholders);
    }

    function extra_options_form(&$form, &$form_state) {
        parent::extra_options_form($form, $form_state);

        $form['granularity'] = $this->date_handler->granularity_form($this->options['granularity']);
        $form['granularity']['#title'] = t('Time ago filter granularity');


        $fields = date_views_fields($this->base_table);
        $options = array();
        foreach ($fields['name'] as $name => $field) {
            $options[$name] = $field['label'];
        }

        $form['date_fields'] = array(
            '#title' => t('Date field(s)'),
            '#type' => 'checkboxes',
            '#options' => $options,
            '#default_value' => $this->options['date_fields'],
            '#multiple' => FALSE,
            '#description' => t('Select date field(s) to filter.'),
            '#required' => TRUE,
        );
        $form['date_method'] = array(
            '#title' => t('Method'),
            '#type' => 'radios',
            '#options' => array('OR' => t('OR'), 'AND' => t('AND')),
            '#default_value' => $this->options['date_method'],
            '#description' => t('Method of handling multiple date fields in the same query. Return items that have any matching date field (date = field_1 OR field_2), or only those with matches in all selected date fields (date = field_1 AND field_2).'),
        );
    }

    function extra_options_validate($form, &$form_state) {
        $check_fields = array_filter($form_state['values']['options']['date_fields']);
        if (empty($check_fields)) {
            form_error($form['date_fields'], t('You must select at least one date field for this filter.'));
        }
    }

    function extra_options_submit($form, &$form_state) {
        $form_state['values']['options']['date_fields'] = array_filter($form_state['values']['options']['date_fields']);
    }

    // Update the summary values to provide
    // meaningful information for each option.
    function admin_summary() {
        if (empty($this->options['date_fields'])) {
            return t('Missing date fields!');
        }
        $handler = $this->date_handler;

        $fields = date_views_fields($this->view->base_table);
        if (!empty($this->options['date_fields'])) {
            $output = array();
            foreach ($this->options['date_fields'] as $field) {
                if (array_key_exists($field, $fields['name'])) {
                    $output[] = $fields['name'][$field]['label'];
                }
            }
        }
        $field = implode(' ' . $this->options['date_method'] . ' ', $output);
        $output = "$field " . check_plain($this->operator) . ' ';
        $parts = $handler->date_parts();
        // If the filter is exposed, display the granularity.
        if ($this->options['exposed']) {
            return t('(@field) <strong>Exposed</strong> @format', array('@field' => $field, '@format' => $parts[$handler->granularity]));
        }
        // If not exposed, display the value.
        if (in_array($this->operator, $this->operator_values(2))) {
            $min = check_plain($this->options['value']['min']);
            $max = check_plain($this->options['value']['max']);
            $output .= t('@min and @max', array('@min' => $min, '@max' => $max));
        } else {
            $output .= check_plain($this->options['value']['value']);
        }
        return $output;
    }

    function get_query_fields() {
        $fields = date_views_fields($this->base_table);
        $fields = $fields['name'];
        $this->query_fields = array();
        foreach ((array) $this->options['date_fields'] as $delta => $name) {
            if (array_key_exists($name, $fields) && $field = $fields[$name]) {
                $date_handler = new date_sql_handler($field['sql_type'], date_default_timezone());
                $date_handler->granularity = $this->options['granularity'];
                $date_handler->db_timezone = date_get_timezone_db($field['tz_handling']);
                $date_handler->local_timezone = date_get_timezone($field['tz_handling']);
                $this->query_fields[] = array('field' => $field, 'date_handler' => $date_handler);
            }
        }
    }

    /**
     * Set the granularity of the date parts to use in the filter.
     */
    function has_extra_options() {
        return TRUE;
    }

}

